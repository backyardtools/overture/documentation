# Backyard Tools: Overture
## Documentation - Formats - PKM

`PKM` files are Guilty Gear 2: Overture's primary file container format.

Most files in the game are stored in and loaded from PKM files.

The game contains many loose files - while some of these are used, a majority are duplicates of files that exist in PKMs and thus are likely leftovers.

With a handful of exceptions, these files usually do not contain filenames.

### Format:

#### Header:

PKM files begin with a header, structured with the following fields:
 - `magic`: A 2-byte identifier, the ASCII string `xf`. (`0x7866`)
 - `endianness`: A 2-byte(?) flag indicating if the file is little endian (`0x00`) or big endian (`0x01`). Only 3 big endian files exist in the PC release.
 - `filecount`: 4-byte integer. Number of files contained in this PKM file
 - `unk1`: 4-byte integer. Unknown. Always `103`.
 - `type`: 2-byte integer. PKM type:
   - `1`: Unknown. Not used by the game.
   - `2`: Contains filenames.
   - `3`: Does not contain filenames.
 - `unk3`: 2-byte integer. Unknown. Always `16`.
  
This header is always directly followed by a series of file information entries, the number of which is equal to `filecount`.

#### File information entry

File information entries contain information on a file in a PKM:
 - `filename`: 256-byte string. Contains the name of the file. This is NOT present in type 3 PKMs.
 - `offset`: 4-byte integer. The file's offset, relative to the start of the data section
 - `size`: 4-byte integer. The file's size in bytes.

There may be alignment to 16-byte boundaries between each information entry.

#### Alignment

Following the file information entry list, there may be padding to align the file to a 64-byte boundary.

#### Data section

Contains all the file data for this PKM. 16-byte boundary alignment between each file's data.