# Backyard Tools: Overture
## Documentation - Formats - MFT

`MFT` files are used by Guilty Gear 2: Overture to store various types of data. Currently under research.

They are defined as tables with rows and columns.

MFTs are found in the following locations, for the following purposes:
 - `data/particle/#quick.MFT`: Unknown but probably something to do with particles.
 - `data/levels/level[xxx]`:
   - `ghost.MFT`: Defines information about the ghosts present in a level, including masterghosts.
   - `LevelSettingTable.MFT`: Defines `lso`/`bin` combinations for level weather
   - `scene2.MFT`: Unknown.

While MFT is a standard format for the game, each MFT serves a different specialised purpose. See the specific pages for each MFT file for information on the specifics of the data each one contains.
  
### Format:

The MFT format is still under research and it has not yet been solved to a readable/writable state.

#### Header:

MFT files begin with a header, structured with the following fields:
 - `magic`: A 4-byte identifier, the ASCII string `MFT ` backwards. (`0x0054464D`)
 - `columns`: 2 byte-integer. Number of table columns.
 - `rows`: 2-byte integer. Number of table rows.
 - `unk1`: 4-byte integer. Unknown. Seems to always be `0x010000000` (1)

### Column IDs:

This header is followed by a list of ints, the count of which matches the column count. These ints are used to identify the purpose of each column.

| ID   | Column name       |
|------|-------------------|
| 0    | index             |
| 1    | index2            |
| 2    | unknown           |
| 3    | unknown           |
| 5    | unknown           |
| 6    | unknown           |
| 8    | unknown           |
| 9    | unknown           |
| 13   | Position X        |
| 14   | Position Y        |
| 15   | Position Z        |
| 16   | Rotation X        |
| 17   | Rotation Y        |
| 18   | Rotation Z        |
| 19   | Scale X           |
| 20   | Scale Y           |
| 21   | Scale Z           |
| 25   | unknown           |
| 104  | unknown           |
| 105  | unknown           |
| 106  | unknown           |
| 107  | Linked Ghost 1    |
| 108  | Linked Ghost 2    |
| 109  | Linked Ghost 3    |
| 110  | Linked Ghost 4    |
| 111  | Linked Ghost 5    |
| 112  | Linked Ghost 6    |
| 115  | unknown           |
| 116  | unknown           |
| 117  | master ghost flag |
| 142  | LSO file          |
| 143  | BIN file          |

### Column types

The column ID block is then followed by another list of ints for each column, this time indicating what type of value that column holds.

| Type ID  | Type      | Description                          |
|----------|-----------|--------------------------------------|
| 0        | Int       | 4 bytes, little-endian integer       |
| 1        | Float     | 4 bytes, floating-point              |
| 2        | NT String | Null-terminated string               |
| 3        | Unknown   | 4 bytes, little-endian integer       |

### Data offsets

In this section, for each row, there is a set of offsets for each column in that row provided as a 4 byte integer. These offsets describe the positions of where the data for that column in that row is located relative to the start of the data section.

### Data section

The column data is stored here at the end of the file.

### Quirks

In the game's files, data in the data section is arranged by column. First with all the data for column 0, then column 1, etc. however since these are accessed via the data offsets their order can be arbitrary. For best compatibility, it is likely ideal to stick with the original ordering scheme when writing MFT files.