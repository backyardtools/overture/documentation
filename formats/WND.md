# Backyard Tools: Overture
## Documentation - Formats - WND

`WND` (World Navigation Data) files are used for Guilty Gear 2: Overture's level AI navigation data. This data is also utilised to determine the "in bounds" area of a level, beyond which players will be reset back to their masterghost.