# Backyard Tools: Overture
## Documentation - Structure - Anatomy of a Level

This file details the structure of the level data in Guilty Gear 2: Overture. This is a high level overview of the directories and files involved. Please see the formats section for specifics on each format.

### Location

Levels are stored in `data/levels` relative to the root of the game directory.

Each level is assigned a 3-digit numerical ID. It is currently unknown how the game populates the ingame level list but it is likely hardcoded. The debug level selector however parses every valid level in this directory.

### Files

The following formats are utilised:
- [LSO](/formats/LSO.md)
- BIN (Level files)
- [MFT](/formats/MFT.md)
- [WCD](/formats/WCD.md)
- [WND](/formats/WND.md)
- [PKM](/formats/PKM.md)

The following files are required for a level to function ingame.

- `LSO files`: Exact purpose unknown but appears to have some jurisdiction over the level's lighting, in conjunction with `BIN` files. In theory, only one of these is needed but each level has several to reflect the different selectable weathers and times of day.
- `BIN files`: Not to be confused with other files that use the same extension. Exact purpose is unknown but appears to relate somehow to time of day and weather, in conjunction with LSO files. Typically every level has a few standard files, but some may have additional files to accompany unique settings. Standard files are:
	- `daytime`
	- `foggy`
	- `morning`
	- `night`
	- `rain`
	- `sunset`
	- `sp`
- `LevelSettingTable.MFT`: MFT file containing the valid combinations of LSO and BIN files, comprising the game's lighting and weather settings for each level.
- `ghost.MFT`: MFT file that contains the data for the level's ghosts, including master ghosts. Seemingly defines everything about them. Specifics unknown due to lack of knowledge on the MFT format itself.
- `scene2.MFT`: Completely unknown at this time. May contain the details of how the map's models are arranged to form its geometry.
- `WORLD.WCD`: Contains the level's collision data,
- `NAVIDB.WND`: Contains navigation data for the level. Also seems to contain data used by the game to determine "out of bounds" areas.
- `MODEL.PKM`: PKM containing all model assets used by the level. Notably, these PKMs are the only type 2 PKMs in the game. See the [PKM format page](/formats/PKM.md) for more information.
- `TEXTURE.PKM`: PKM containing all texture assets used by the level.
- `MOTION.PKM`: Unknown. Likely animation-related.
- `MOTIONMFT.PKM`: Unknown. Certainly doesn't contain any MFT files.